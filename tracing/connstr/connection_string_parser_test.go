package connstr

import (
	"reflect"
	"testing"
)

func Test_Parse(t *testing.T) {
	tests := []struct {
		name             string
		connectionString string
		wantDriverName   string
		wantOptions      map[string]string
		wantErr          bool
	}{
		{
			name:             "empty",
			connectionString: "",
			wantErr:          true,
		},
		{
			name:             "invalid",
			connectionString: "notopentracing://something",
			wantErr:          true,
		},
		{
			name:             "invalidport",
			connectionString: "opentracing://something:port",
			wantErr:          true,
		},
		{
			name:             "jaeger",
			connectionString: "opentracing://jaeger",
			wantDriverName:   "jaeger",
			wantOptions:      map[string]string{},
			wantErr:          false,
		},
		{
			name:             "datadog",
			connectionString: "opentracing://datadog",
			wantDriverName:   "datadog",
			wantOptions:      map[string]string{},
			wantErr:          false,
		},
		{
			name:             "lightstep",
			connectionString: "opentracing://lightstep?api_key=123",
			wantDriverName:   "lightstep",
			wantOptions: map[string]string{
				"api_key": "123",
			},
			wantErr: false,
		},
		{
			name:             "path",
			connectionString: "opentracing://lightstep/123?api_key=123",
			wantErr:          true,
		},
		{
			name:             "multiple_params",
			connectionString: "opentracing://lightstep?api_key=123&host=localhost:1234",
			wantDriverName:   "lightstep",
			wantOptions: map[string]string{
				"api_key": "123",
				"host":    "localhost:1234",
			},
			wantErr: false,
		},
		{
			name:             "repeat_params",
			connectionString: "opentracing://lightstep?api_key=123&host=localhost:1234&host=moo",
			wantDriverName:   "lightstep",
			wantOptions: map[string]string{
				"api_key": "123",
				"host":    "localhost:1234",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotDriverName, getOptions, err := Parse(tt.connectionString)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseConnectionString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(gotDriverName, tt.wantDriverName) {
				t.Errorf("ParseConnectionString().driverName = %v, want %v", gotDriverName, tt.wantDriverName)
			}

			if !reflect.DeepEqual(getOptions, tt.wantOptions) {
				t.Errorf("ParseConnectionString().options = %v, want %v", getOptions, tt.wantOptions)
			}
		})
	}
}
